package model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TableroTest {

    private Tablero tablero;

    @BeforeEach
    void setUp() {
        // inicializamos la clase
        tablero = new Tablero();
    }

    @Test
    void testGetPosicion() {
        assertEquals("1", tablero.getPosicion(1));
    }

    @Test
    void testPonerFicha() {
        tablero.ponerFicha(1, Ficha.X);
        assertEquals("X", tablero.getPosicion(1));
    }

    @Test
    void testPosicionLibreTrue() {
        assertTrue(tablero.posicionLibre(1));
    }

    @Test
    void testPosicionLibreFalse() {
        tablero.ponerFicha(1, Ficha.X);
        assertFalse(tablero.posicionLibre(1));
    }

    @Test
    void testEstaLleno() {
        assertFalse(tablero.estaLleno());
    }

    @Test
    void testHayGanadorFalse() {
        assertFalse(tablero.hayGanador());
    }

    @Test
    void testHayGanadorTrue() {
        tablero.ponerFicha(1, Ficha.X);
        tablero.ponerFicha(2, Ficha.X);
        tablero.ponerFicha(3, Ficha.X);
        assertTrue(tablero.hayGanador());
    }
}