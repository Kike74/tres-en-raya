

import view.Menu;

public class Main {

    private Menu menu;

    public static void main(String[] args) {
        Main main = new Main();
        main.init();
    }

    private void init() {
        menu = new Menu();
    }
}