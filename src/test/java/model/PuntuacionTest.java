package model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PuntuacionTest {

    private Puntuacion puntuacion;

    @BeforeEach
    void setUp(){
        // inicializamos la clase
        puntuacion = new Puntuacion();
    }

    @Test
    void testGanaX() {
        puntuacion.ganaX();
        assertEquals(1, puntuacion.getGanaX());
    }

    @Test
    void testGanaO() {
        puntuacion.ganaO();
        assertEquals(1, puntuacion.getGanaO());
    }

    @Test
    void testGetGanaX() {
        assertEquals(0, puntuacion.getGanaX());
    }

    @Test
    void testGetGanaO() {
        assertEquals(0, puntuacion.getGanaO());
    }

    @Test
    void testGeset() {
        puntuacion.reset();
        assertEquals(0, puntuacion.getGanaX());
        assertEquals(0, puntuacion.getGanaO());
    }
}