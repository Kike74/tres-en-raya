package model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TurnoTest {

    private Turno turno;

    @BeforeEach
    void setUp() {
        // inicializamos la clase
        turno = new Turno(Ficha.X);
    }

    @Test
    void testCambiarTurno() {
        turno.cambiarTurno();
        assertEquals(Ficha.O, turno.getFicha());
    }

    @Test
    void testGetFicha() {
        assertEquals(Ficha.X, turno.getFicha());
    }

    @Test
    void testToString1() {
        assertEquals("X", turno.toString());
    }
}